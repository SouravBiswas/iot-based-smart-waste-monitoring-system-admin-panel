<?php include 'inc/header.php'?>

    <script src="jquery.min.js"></script>
  <style>
      #map {
          width: 1040px;
          height: 450px;
          border: 1px solid blue;
      }


  </style>


    <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="index.php">Dashboard</a>
        </li>
       &nbsp;  <a href="view_map.php" style="color: black"> All Bins Location<a/>
      &nbsp;  <a href="shortestPath.php" style="color: black;font-weight: bold"> Get Shortest Path<a/> 
      </ol>
      <!-- Example DataTables Card-->
      <div class="card mb-3">
        <div class="card-header">
          <i class="fa fa-map-marker"></i> View Map </div>
        
        <div class="card-body">


            <script src='https://maps.google.com/maps/api/js?key=AIzaSyA8QNn_Z3SYp0-3qtdZ_JHkLcSLrPBjfEw&callback=showMap' type='text/javascript'></script>
            <script type='text/javascript'>
                (function(){
                    
                   //var poly = new GPolyline([new GLatLng(22.3508,91.8423), new GLatLng(22.3412,91.8361)],"#ff0000",10);
                    //map.addOverlay(poly);

                    var map,marker,latlng,bounds,infowin;
                    /* initial locations for map */
                    var _lat=22.3508;
                    var _lng=91.8423;

                    var getacara=0; /* What should this be? is it a function, a variable ???*/

                    function showMap(){
                        /* set the default initial location */
                        latlng={ lat: _lat, lng: _lng };

                        bounds = new google.maps.LatLngBounds();
                        infowin = new google.maps.InfoWindow();

                        /* invoke the map */
                        map = new google.maps.Map( document.getElementById('map'), {
                            center:latlng,
                            zoom: 25
                        });

                        /* show the initial marker */
                        marker = new google.maps.Marker({
                            position:latlng,
                            map: map,
                            title: 'Hello World!'
                        });
                        bounds.extend( marker.position );

                        /* I think you can use the jQuery like this within the showMap function? */
                        $.ajax({
                            /*
                             I'm using the same page for the db results but you would
                             change the below to point to your php script ~ namely
                             phpmobile/getlanglong.php
                             */
                            url:'get_locations.php',
                            data: { 'id': getacara, 'ajax':true },
                            dataType: 'json',
                            success: function( data, status ){
                                $.each( data, function( i,item ){
                                    /* add a marker for each location in response data */
                                    var value= item.description +' , '+ item.address;
                                    var ultra_value = item.level;
                                    //   console.log(value);
                                    addMarker( item.latitude, item.longitude,value,ultra_value );
                                    
                                });
                            },
                            error: function(){
                                output.text('There was an error loading the data.');
                            }
                        });
                    }
                    
                    
                    

                    /* simple function just to add a new marker */
                    function addMarker(lat,lng,title,ultra_value){
                        value= ultra_value;
                       // var polyline = new GPolyline([new GLatLng(lat,lng)],"#ff0000",10);
                             
                       // console.log(value);
                        if(value>=80){
                            marker = new google.maps.Marker({/* Cast the returned data as floats using parseFloat() */
                                position:{ lat:parseFloat( lat ), lng:parseFloat( lng ) },
                                map:map,
                                title:title,
                                // draggable: true, // enables drag & drop
                                animation: google.maps.Animation.BOUNCE
                              //  map.addOverlay(polyline);
                             
                             
                             });
                             
                                
                        }else {
                           // var img = new Image();
                           // green_image = img.src = "image/green_marker.png";
                            marker = new google.maps.Marker({/* Cast the returned data as floats using parseFloat() */
                                position:{ lat:parseFloat( lat ), lng:parseFloat( lng ) },
                                map:map,
                                title:title,
                                draggable: true
                             //   icon:green_image
                                // enables drag & drop
                                //   animation: google.maps.Animation.DROP,


                            });
                        }
                            

                        google.maps.event.addListener( marker, 'click', function(event){
                            infowin.setContent(this.title);
                            infowin.open(map,this);
                            infowin.setPosition(this.position);
                        }.bind( marker ));

                        bounds.extend( marker.position );
                        map.fitBounds( bounds );

                    }
                    

                    function toggleBounce() {
                        if (marker.getAnimation() !== null) {
                            marker.setAnimation(null);
                        } else {
                            marker.setAnimation(google.maps.Animation.BOUNCE);
                        }
                    }


                    document.addEventListener( 'DOMContentLoaded', showMap, false );
                }());
            </script>
            <div id='map'></div>

        </div>
        <div class="card-footer small text-muted">Updated yesterday at 11:59 PM</div>
      </div>
    </div>



        <!-- /.container-fluid-->
    <!-- /.content-wrapper-->
<?php include 'inc/footer.php'?>