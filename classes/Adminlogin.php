<?php
$filepath = realpath(dirname(__FILE__));
include_once ($filepath.'/../lib/Session.php');
//include '../lib/Session.php';
Session::checkLogin();

include_once ($filepath.'/../lib/Database.php');

?>

<?php


class Adminlogin
{

    private $db;

    public function __construct(){

        $this->db = new Database();


    }

    public function adminLogin($adminEmail,$adminPass){


        if(empty($adminEmail) || empty($adminPass)){

            $loginmsg = "Username or Password must not be empty";
            return $loginmsg;
        }else{

            $query = "select * from user WHERE email='$adminEmail' AND password='$adminPass'";
            $query = $this->db->select($query);
            $result = mysqli_num_rows($query);

            if($result){
                $value = mysqli_fetch_assoc($query);
                Session::set("adminlogin",true);
                Session::set("adminId",$value['id']);
                Session::set("adminEmail",$value['email']);
                Session::set("adminName",$value['name']);
                Session::set("adminPass",$value['password']);
                if($value['status']==0){
                   echo "<script type='text/javascript'>  window.location='index.php'; </script>";
                }else{
                    echo "<script type='text/javascript'>  window.location='UserPanel/index.php'; </script>";
                }
            }else{
                $loginmsg = "Username or Password not match";
                return $loginmsg;
            }
        }
    }
    
      public function binInsert($bindescription,$binaddress,$binlatitude,$binlongitude){

        if($bindescription !="" && $binaddress!="" && $binlatitude!="" && $binlongitude!=""){
            $query = "insert into SAVE_DATA(description,address,latitute,logitute) VALUES ('$bindescription','$binaddress','$binlatitude','$binlongitude')";
            $binInsert = $this->db->insert($query);
            if($binInsert){
                $msg = "<center><span id='message' class='success' style='font-weight: bold;font-size: 15px;color:green'>Data Inserted Successfully!!</span></center>";
                return $msg;
            }else{
                $msg = "<center><span id='message' class='error' style='font-weight: bold;font-size: 15px;color:red'>Data not Inserted !!</span></center>";
                return $msg;
            }
        }else{
            $msg = "<center><span id='' class='error' style='font-weight: bold;font-size: 15px;color:red'>Field must not be empty!!</span></center>";
            return $msg;
        }


    }
    
      public function getAllBin(){

        $query = "select * from SAVE_DATA ";
        $result = $this->db->select($query);
        //var_dump($result);
        return $result;
    }
}
?>