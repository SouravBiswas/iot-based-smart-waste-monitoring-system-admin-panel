<?php include 'inc/header.php'?>
<?php include 'classes/Adminlogin.php'?>
<?php
$obj = new Adminlogin();
if($_SERVER['REQUEST_METHOD'] == 'POST'){
    $bindescription = $_POST['bindescription'];
    $binaddress = $_POST['binaddress'];
    $binlatitude = $_POST['binlatitude'];
    $binlongitude = $_POST['binlongitude'];
    $insertbin = $obj->binInsert($bindescription,$binaddress,$binlatitude,$binlongitude);
}
?>
  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="#">Dashboard</a>
        </li>
        <li class="breadcrumb-item active">Charts</li>
      </ol>
      <!-- Area Chart Example-->
      <div class="card mb-3">
        <div class="card-header">
          <i class="fa fa-fw fa-bitbucket-square"></i>  Add Bin</div>
        <div class="card-body">
            <center>
           <div class="block copyblock">
                <?php
                if(isset($insertbin)){
                    echo $insertbin;
                }
                ?>
                <form action="" method="post">
                    <table class="form" style="height: 250px">
                        <tr>
                            <td>
                                <input style="width: 350px" type="text" name="bindescription" placeholder="Enter Bin Description..." class="form-control" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <input style="width: 350px" type="text" name="binaddress" placeholder="Enter Bin Address..." class="form-control" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <input style="width: 350px" type="text" name="binlatitude" placeholder="Enter Bin Latitute..." class="form-control" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <input style="width: 350px" type="text" name="binlongitude" placeholder="Enter Bin Longitute..." class="form-control" />
                            </td>
                        </tr>
                        
                        <tr>
                            <td>
                                <input type="submit" name="submit" Value="Save" />
                            </td>
                        </tr>
                    </table>
                </form>
            </div>
            </center>
        </div>
        <div class="card-footer small text-muted">Updated yesterday at 11:59 PM</div>
      </div>
      <div class="row">
       
         
       </div>
      </div>
    </div>
    <!-- /.container-fluid-->
    <!-- /.content-wrapper-->
<?php include 'inc/footer.php'?>