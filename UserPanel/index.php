<?php include 'inc/header.php'?>
<?php include '../classes/Bin.php'?>
<?php
$obj = new Bin();
$allbin = $obj->getAllBins();
$number = mysqli_num_rows($allbin);

?>
    <script src="vendor/jquery/raphael-2.1.4.min.js"></script>
    <script src="vendor/jquery/justgage.js"></script>
  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumbs-->
     
     
      <!-- Area Chart Example-->
      <div class="card mb-3">
        <div class="card-header">
          <i class="fa fa-bitbucket" ></i> Priority Base Bin Sequence</div>
        <div class="card-body">
      
                   <center>
                    <table class="data display datatable" id="example" border="2px">
                        
					<thead>
					    <?php
					    if($number!=0){
					    ?>
						<tr style="text-align: center">
							<th>Serial No.</th>
							<th>Bin Name</th>
                            <th>Bin Location</th>
							<th>Action</th>
							
						</tr>
					</thead>
				
					<tbody>
                    <?php
                    
                        $i=0;
                        while ($result = mysqli_fetch_assoc($allbin)){
                            $i++;
                    ?>
						<tr class="odd gradeX" style="text-align: center">
							<td><?php echo $i?></td>
							<td width="85px"><?php echo $result['description']?></td>
                            <td width="350px"><?php echo $result['address']?></td>
							<td width="100px"><a href="view_map.php">See on Map</a></td>
						</tr>
                    <?php } }else{
                       $msg = "<center><span id='message' class='error' style='font-weight: bold;color: red;text-align:center;font-size: 18px'>No Fill Up Bin Found Yet !!</span></center>";
                       echo $msg;
                    }
                    
                    ?>
					</tbody>
				</table>
				</center>
               

      </div>
      

    </div>
    <!-- /.container-fluid-->
    <!-- /.content-wrapper-->
   <?php include "inc/footer.php";?>