<?php
$filepath = realpath(dirname(__FILE__));

include_once ($filepath.'/../lib/Database.php');
include_once ($filepath.'/../helpers/Format.php');


class Brand
{
    private $db;
    private $fm;
    public function __construct(){

        $this->db = new Database();
        $this->fm = new Format();

    }

    public function brandInsert($brandName,$brandStatus){

        $brandName = $this->fm->validation($brandName);
        $brandStatus = $this->fm->validation($brandStatus);

        $brandName = mysqli_real_escape_string($this->db->link,$brandName);
        $brandStatus = mysqli_real_escape_string($this->db->link,$brandStatus);
        if(empty($brandName) || empty($brandStatus) ){

            $msg = "<span class='error'>Field must not be empty !!</span>";
            return $msg;
        }else{

            $query = "insert into tbl_brands(brandName,brand_active,brand_status) VALUES ('$brandName','$brandStatus',1)";
            $brandInsert = $this->db->insert($query);
            if($brandInsert){
                $msg = "<center><span id='message' class='success' style='font-weight: bold;font-size: 15px'>Brand Inserted Successfully!!</span></center>";
                return $msg;
            }else{
                $msg = "<center><span id='message' class='error' style='font-weight: bold;font-size: 15px'>Brand not Inserted !!</span></center>";
                return $msg;
            }
        }

    }
    public function getAllBrand(){

        $query = "select * from tbl_brands order by brandId DESC ";
        $result = $this->db->select($query);

        return $result;
    }

    public function getbrandById($id){

        $query = "select * from tbl_brands where brandId = '$id'";
        $result = $this->db->select($query);

        return $result;
    }


    public function brandUpdate($brandName,$brandStatus,$id){

        $brandName = $this->fm->validation($brandName);
        $brandStatus = $this->fm->validation($brandStatus);

        $brandName = mysqli_real_escape_string($this->db->link,$brandName);
        $brandStatus = mysqli_real_escape_string($this->db->link,$brandStatus);
        $id = mysqli_real_escape_string($this->db->link,$id);
        if(empty($brandName) || empty($brandStatus)){

            $msg = "<span class='error'>Field must not be empty !!</span>";
            return $msg;
        }else{

            $query = "update tbl_brands set brandName='$brandName',brand_active = '$brandStatus' where brandId='$id'";
            $result = $this->db->update($query);
            if($result){
                $msg = "<center><span id='message' class='success' style='font-weight: bold;font-size: 15px'>Brand Updated Successfully !!</span></center>";
                return $msg;
            }
            else{
                $msg = "<center><span id='message' class='error' style='font-weight: bold;font-size: 15px'>Brand not Updated !!</span></center>";
                return $msg;
            }


        }
    }


    public function delBrandById($id){

        $query = "delete from tbl_brands where brandId = '$id'";
        $result = $this->db->delete($query);
        if($result){
            $msg = "<center><span id='message' class='success' style='font-weight: bold;font-size: 15px'>Brand Deleted Successfully !!</span></center>";
            return $msg;
        }else{
            $msg = "<center><span id='message' class='error' style='font-weight: bold;font-size: 15px'>Brand not Deleted !!</span></center>";
            return $msg;
        }
    }

    public function supplierInsert($supplierName,$supplierContact,$supplierProduct){

        if($supplierName !="" && $supplierContact!="" && $supplierProduct!=""){
            $query = "insert into tbl_supplier(supplier_name,supplier_contact,supplier_product) VALUES ('$supplierName','$supplierContact','$supplierProduct')";
            $supplierInsert = $this->db->insert($query);
            if($supplierInsert){
                $msg = "<center><span id='message' class='success' style='font-weight: bold;font-size: 15px'>Data Inserted Successfully!!</span></center>";
                return $msg;
            }else{
                $msg = "<center><span id='message' class='error' style='font-weight: bold;font-size: 15px'>Data not Inserted !!</span></center>";
                return $msg;
            }
        }else{
            $msg = "<center><span id='' class='error' style='font-weight: bold;font-size: 15px'>Field must not be empty!!</span></center>";
            return $msg;
        }


    }

    public function delSupplierById($id){

        $query = "delete from tbl_supplier where supplier_id = '$id'";
        $result = $this->db->delete($query);
        if($result){
            $msg = "<center><span id='message' class='success' style='font-weight: bold;font-size: 15px'>Supplier Deleted Successfully !!</span></center>";
            return $msg;
        }else{
            $msg = "<center><span id='message' class='error' style='font-weight: bold;font-size: 15px'>Supplier not Deleted !!</span></center>";
            return $msg;
        }
    }

    public function getAllSupplier(){

        $query = "select * from tbl_supplier";
        $result = $this->db->select($query);

        return $result;
    }


    public function getSupplierProduct($productId){

        $query = "select tbl_product.productName from tbl_product where productId = '$productId'";
        $result = $this->db->select($query);

        return $result;

           }

           public function supplierUpdate($supplierName,$supplierContact,$supplierProduct,$id){

               $query = "update tbl_supplier set supplier_name='$supplierName',supplier_contact = '$supplierContact',supplier_product = '$supplierProduct' where supplier_id='$id'";
               $result = $this->db->update($query);
               if($result){
                   $msg = "<center><span id='message' class='success' style='font-weight: bold;font-size: 15px'>Data Updated Successfully !!</span></center>";
                   return $msg;
               }
               else{
                   $msg = "<center><span id='message' class='error' style='font-weight: bold;font-size: 15px'>Data not Updated !!</span></center>";
                   return $msg;
               }

           }

           public function getSupplierById($id){

               $query = "select * from tbl_supplier where supplier_id = '$id'";
               $result = $this->db->select($query);

               return $result;
           }





}