<?php
$filepath = realpath(dirname(__FILE__));

include_once ($filepath.'/../lib/Database.php');
include_once ($filepath.'/../helpers/Format.php');

class Customer
{

    private $db;
    private $fm;
    public function __construct(){

        $this->db = new Database();
        $this->fm = new Format();

    }

    public function customerRegistration($data){
        $name = mysqli_real_escape_string($this->db->link, $data['name']);
        $address = mysqli_real_escape_string($this->db->link, $data['address']);
        $city = mysqli_real_escape_string($this->db->link, $data['city']);
        $country = mysqli_real_escape_string($this->db->link, $data['country']);
        $zip = mysqli_real_escape_string($this->db->link, $data['zip']);
        $phone = mysqli_real_escape_string($this->db->link, $data['phone']);
        $email = mysqli_real_escape_string($this->db->link, $data['email']);
        $pass = mysqli_real_escape_string($this->db->link, $data['pass']);
        $pass = md5($pass);
        if ($name == "" || $address == "" || $city == "" || $country == "" || $zip == "" || $phone == "" || $email == "" || $pass == "") {
            $msg = "<span class='error' id='message' style='font-weight: bold;color: red'>Fields must not be empty !!</span>";
            return $msg;
        }
        $mailQuery = "select * from tbl_customer where email = '$email' limit 1";
        $mailChk = $this->db->select($mailQuery);
        if ($mailChk != false){
            $msg = "<span class='error' id='message' style='font-weight: bold;color:red;'>Email already exist !!</span>";
            return $msg;
        }else{
            $query = "insert into tbl_customer(name,address,city,country,zip,phone,email,pass) values('$name','$address','$city','$country','$zip','$phone','$email','$pass')";
            $customerInsert = $this->db->insert($query);
            if ($customerInsert) {
                $msg = "<center><span id='message' class='success' style='font-weight: bold;color:green;text-align:center;font-size: 15px'>Registration Completed!!Cheak your email to varify your account!!</span></center>";
                return $msg;
            } else {
                $msg = "<center><span id='message' class='error' style='font-weight: bold;color:red;text-align:center;font-size: 15px'>Customer Information not Inserted !!</span></center>";
                return $msg;
            }

        }

    }

    public function customerLogin($data){
        $email = mysqli_real_escape_string($this->db->link, $data['email']);
        $pass = mysqli_real_escape_string($this->db->link, md5($data['pass']));
        if(empty($email) || empty($pass)){
            $msg = "<span class='error' id='message' style='font-weight: bold;color: red'>Fields must not be empty !!</span>";
            return $msg;
        }
        $query = "select * from tbl_customer where email = '$email' and pass = '$pass'";
        $result = $this->db->select($query);
        if ($result != false){
            $value = mysqli_fetch_assoc($result);
            Session::set('customerLogin',true);
            Session::set('customerId',$value['id']);
            Session::set('customerName',$value['name']);
            Session::set('customerEmail',$value['email']);
            header("Location:cart.php");
        }else{
            $msg = "<span class='error' id='message' style='font-weight: bold;color: red'>Email or Password incorrect!!</span>";
            return $msg;
        }


    }

    public function getCustomerData($id){

        $query = "select * from tbl_customer where id = '$id'";
        $result = $this->db->select($query);
        return $result;


    }

    public function customerUpdate($data,$customerId){
        $name = mysqli_real_escape_string($this->db->link, $data['name']);
        $address = mysqli_real_escape_string($this->db->link, $data['address']);
        $city = mysqli_real_escape_string($this->db->link, $data['city']);
        $country = mysqli_real_escape_string($this->db->link, $data['country']);
        $zip = mysqli_real_escape_string($this->db->link, $data['zip']);
        $phone = mysqli_real_escape_string($this->db->link, $data['phone']);
        $email = mysqli_real_escape_string($this->db->link, $data['email']);
        if ($name == "" || $address == "" || $city == "" || $country == "" || $zip == "" || $phone == "" || $email == "") {
            $msg = "<span class='error' id='message' style='font-weight: bold;color: red'>Fields must not be empty !!</span>";
            return $msg;
        }
        else{

            $query = "update tbl_customer set name='$name',address='$address',city='$city',country='$country',zip='$zip',phone='$phone',email='$email' where id='$customerId'";
            $customerUpdate = $this->db->update($query);
            if ($customerUpdate) {
                $msg = "<center><span id='message' class='success' style='font-weight: bold;font-size: 15px;color: green'>Your Profile Updated Successfully!!</span></center>";
                return $msg;
            } else {
                $msg = "<center><span id='message' class='error' style='font-weight: bold;font-size: 15px;color: red'>Your Profile not Updated !!</span></center>";
                return $msg;
            }

        }
    }

    public function getAllCustomer(){
        $query = "select * from tbl_customer ";
        $result = $this->db->select($query);
        return $result;

    }

    public function getAdminInfo($adminId){
        $query = "select * from tbl_admin where adminId = '$adminId' ";
        $result = $this->db->select($query);
        return $result;

    }

    public function adminUpdate($data,$adminId){
        $name = mysqli_real_escape_string($this->db->link, $data['adminName']);
        $userName = mysqli_real_escape_string($this->db->link, $data['adminUser']);
        $email = mysqli_real_escape_string($this->db->link, $data['adminEmail']);
        if ($name == "" || $userName == "" || $email == "") {
            $msg = "<span class='error' id='message' style='font-weight: bold;color: red'>Fields must not be empty !!</span>";
            return $msg;
        }
        else{

            $query = "update tbl_admin set adminName='$name',adminUser='$userName',adminEmail='$email' where adminId='$adminId'";
            $adminUpdate = $this->db->update($query);
            if ($adminUpdate) {
                $msg = "<center><span id='message' class='success' style='font-weight: bold;font-size: 15px;color: green'>Your Profile Updated Successfully!!</span></center>";
                return $msg;
            } else {
                $msg = "<center><span id='message' class='error' style='font-weight: bold;font-size: 15px;color: red'>Your Profile not Updated !!</span></center>";
                return $msg;
            }

        }

    }

    public function updateAdminPass($data,$adminId){
        $oldPass = mysqli_real_escape_string($this->db->link, md5($data['oldpass']));
        //var_dump($oldPass);
        $newpass = mysqli_real_escape_string($this->db->link, md5($data['newpass']));
        if ($oldPass == "" || $newpass == "" ) {
            $msg = "<span class='error' id='message' style='font-weight: bold;color: red'>Fields must not be empty !!</span>";
            return $msg;
        }else{
            $query = "select * from tbl_admin where adminId = '$adminId' ";
            $result = $this->db->select($query);
            $result = mysqli_fetch_assoc($result);
            if ($result['adminPass'] == $oldPass) {
                $query = "update tbl_admin set adminpass='$newpass'";
                $adminPassUpdate = $this->db->update($query);
                if ($adminPassUpdate) {
                    $msg = "<center><span id='message' class='success' style='font-weight: bold;font-size: 15px;color: green'>Your Password Updated Successfully!!</span></center>";
                    return $msg;
                } else {
                    $msg = "<center><span id='message' class='error' style='font-weight: bold;font-size: 15px;color: red'>Your Password not Updated !!</span></center>";
                    return $msg;
                }
            } else {
                $msg = "<center><span id='message' class='error' style='font-weight: bold;font-size: 15px;color: red'>Your old Password isn't Match !!</span></center>";
                return $msg;
            }

        }
    }

    public function getAllOrder(){
        $sql = "SELECT order_id, order_date, client_name, client_contact, payment_status FROM tbl_order WHERE order_status = 1";
        $result = $this->db->select($sql);
        return $result;
    }

    public function countOrderItem($orderId){

        $countOrderItemSql = "SELECT * FROM order_item WHERE order_id = $orderId";
        $itemCountResult = $this->db->select($countOrderItemSql);
        $itemCountResult = mysqli_num_rows($itemCountResult);
        return $itemCountResult;

    }

    public function allOrderDataById($orderid){

        $sql = "SELECT tbl_order.order_id, tbl_order.order_date, tbl_order.client_name, tbl_order.client_contact, tbl_order.sub_total, tbl_order.vat, tbl_order.total_amount, tbl_order.discount, tbl_order.grand_total, tbl_order.paid, tbl_order.due, tbl_order.payment_type, tbl_order.payment_status FROM tbl_order 	
					WHERE tbl_order.order_id = $orderid";

        $result = $this->db->select($sql);
        return $result;

    }

    public function orderItem($orderid){
        $orderItemSql = "SELECT order_item.order_item_id, order_item.order_id, order_item.product_id, order_item.quantity, order_item.rate, order_item.total FROM order_item WHERE order_item.order_id = {$orderid}";
        $orderItemResult = $this->db->select($orderItemSql);
        return $orderItemResult;

    }

    public function getPaymentById($paymentid){

        $sql = "SELECT * FROM tbl_order WHERE order_id = $paymentid";
        $result = $this->db->select($sql);
        return $result;

    }

    public function paymentUpdate($data,$paymentid){

        $payAmount = $data['paid'];
        $paymentType = $data['paymentType'];
        $paymentStatus = $data['paymentStatus'];

        $sql = "SELECT * FROM tbl_order WHERE order_id = $paymentid";
        $result = $this->db->select($sql);
        if($result){
            while ($datarow = mysqli_fetch_assoc($result)){
                 $paid = $datarow['paid'];
                 $due  = $datarow['due'];
                 $payType = $datarow['payment_type'];
                $payStatus = $datarow['payment_status'];
                $grandTotal = $datarow['grand_total'];
            }

            $updatePaidAmount = $paid + $payAmount;
            $updateDue = $grandTotal - $updatePaidAmount;

            $updateSql = "UPDATE tbl_order SET paid = '$updatePaidAmount', due = '$updateDue', payment_type = '$paymentType', payment_status = '$paymentStatus' WHERE order_id = {$paymentid}";
            $paymentUpdate = $this->db->update($updateSql);
            if ($paymentUpdate) {
                $msg = "<center><span id='message' class='success' style='font-weight: bold;font-size: 15px;color: green'>Payment Updated Successfully!!</span></center>";
                return $msg;
            } else {
                $msg = "<center><span id='message' class='error' style='font-weight: bold;font-size: 15px;color: red'>Payment not Updated !!</span></center>";
                return $msg;
            }

            // var_dump($updatePaidAmount);
           // die();
        }
       // return $result;


    }

    public function delOrderById($id){

        $query = "delete from tbl_order where order_id = '$id'";
        $result = $this->db->delete($query);
        if($result){
            $orderItem = "UPDATE order_item SET order_item_status = 2 WHERE  order_id = {$id}";
            $update = $this->db->update($orderItem);
            if($update){
                $msg = "<center><span id='message' class='success' style='font-weight: bold;font-size: 15px;color: green'>Order Deleted Successfully!!</span></center>";
                return $msg;
            }else{
                $msg = "<center><span id='message' class='error' style='font-weight: bold;font-size: 15px;color: red'>Order not Deleted !!</span></center>";
                return $msg;
            }

        }


    }

    public function getAllOrderCount(){
        $sql = "SELECT * FROM tbl_order WHERE order_status = 1";
        $result = $this->db->select($sql);
        return $result;

    }

    public function stockChecker(){
        $lowStockSql = "SELECT * FROM tbl_product WHERE quantity <= 3 AND status = 1";
        $lowStockQuery = $this->db->select($lowStockSql);
        $countStock = mysqli_num_rows($lowStockQuery);
        return $countStock;



    }

    public function calculateRevenue(){
        $orderSql = "SELECT * FROM tbl_order WHERE order_status = 1";
        $orderQuery = $this->db->select($orderSql);
        return $orderQuery;

    }


}