<?php
if( $_SERVER['REQUEST_METHOD']=='GET' && isset( $_GET['ajax'] ) ){

    $dbhost =   'localhost';
    $dbuser =   'id1522084_ultra_test';
    $dbpwd  =   'ultra_test';
    $dbname =   'id1522084_ultra_test';
    $db     =   new mysqli( $dbhost, $dbuser, $dbpwd, $dbname );

    $places=array();

    $sql    =   'select 
                        `address` as \'address\',
                        `latitute` as \'lat\',
                        `logitute` as \'lng\',
                        `description` as \'description\',
                        `level` as \'level\'
                        from `SAVE_DATA`
                        limit 100';

    $res    =   $db->query( $sql );
    
    if( $res ) while( $rs=$res->fetch_object() ) $places[]=array( 'latitude'=>$rs->lat, 'longitude'=>$rs->lng, 'address'=>$rs->address,'description'=>$rs->description,'level'=>$rs->level );
    $db->close();

    header( 'Content-Type: application/json' );
    echo json_encode( $places,JSON_FORCE_OBJECT );
    exit();
}
?>