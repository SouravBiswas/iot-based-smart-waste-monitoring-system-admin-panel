<?php include 'inc/header.php'?>
<?php
include 'classes/Bin.php';
$obj = new Bin();
$allbin = $obj->getallbin();
$countBin = mysqli_num_rows($allbin);
$alluser = $obj->getalluser();
$countUser = mysqli_num_rows($alluser);

?>
    <script src="vendor/jquery/raphael-2.1.4.min.js"></script>
    <script src="vendor/jquery/justgage.js"></script>
  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="#">Dashboard</a>
        </li>
        <li class="breadcrumb-item active">My Dashboard</li>
      </ol>
      <!-- Icon Cards-->
      <div class="row">
        <div class="col-xl-3 col-sm-6 mb-3">
          <div class="card text-white bg-primary o-hidden h-100">
            <div class="card-body">
              <div class="card-body-icon">
                <i class="fa fa-fw fa-bitbucket-square"></i>
              </div>
              <div class="mr-5"> Total Bins </div><?php echo $countBin;?>
            </div>
            <a class="card-footer text-white clearfix small z-1" href="#">
              <span class="float-left">View Details</span>
              <span class="float-right">
                <i class="fa fa-angle-right"></i>
              </span>
            </a>
          </div>
        </div>
        <div class="col-xl-3 col-sm-6 mb-3">
          <div class="card text-white bg-warning o-hidden h-100">
            <div class="card-body">
              <div class="card-body-icon">
                <i class="fa fa-fw fa-user"></i>
              </div>
              <div class="mr-5"> Total User </div><?php echo $countUser;?>
            </div>
            <a class="card-footer text-white clearfix small z-1" href="#">
              <span class="float-left">View Details</span>
              <span class="float-right">
                <i class="fa fa-angle-right"></i>
              </span>
            </a>
          </div>
        </div>
        <div class="col-xl-3 col-sm-6 mb-3">
          <div class="card text-white bg-success o-hidden h-100">
            <div class="card-body">
              <div class="card-body-icon">
                <i class="fa fa-fw fa-user-circle"></i>
              </div>
              <div class="mr-5"> Total Employee </div>30
            </div>
            <a class="card-footer text-white clearfix small z-1" href="#">
              <span class="float-left">View Details</span>
              <span class="float-right">
                <i class="fa fa-angle-right"></i>
              </span>
            </a>
          </div>
        </div>
        <div class="col-xl-3 col-sm-6 mb-3">
          <div class="card text-white bg-danger o-hidden h-100">
            <div class="card-body">
              <div class="card-body-icon">
                <i class="fa fa-fw fa-stack-exchange"></i>
              </div>
              <div class="mr-5"> Statistics </div>
            </div>
            <a class="card-footer text-white clearfix small z-1" href="#">
              <span class="float-left">View Details</span>
              <span class="float-right">
                <i class="fa fa-angle-right"></i>
              </span>
            </a>
          </div>
        </div>
      </div>
      <!-- Area Chart Example-->
      <div class="card mb-3">
        <div class="card-header">
          <i class="fa fa-bitbucket" ></i> Bin Status </div>
        <div class="card-body">

            <div id="g1"></div>
            <div id="g2"></div>
           

            <script id="source" language="javascript" type="text/javascript">
                var g1, g2,g3;
                var fish=0;
                window.onload = function(){
                    var g1 = new JustGage({
                        id: "g1",
                        value: fish,
                        min: 0,
                        max: 100, //need to replace dynamically
                        title: "Bin1",//need to replace dynamically
                        label: "Load"
                    });

                    var g2 = new JustGage({
                        id: "g2",
                        value: fish,
                        min: 0,
                        max: 100,//need to replace dynamically
                        title: "Bin2",//need to replace dynamically
                        label: "Load"
                    });

                    

// this sends the variable 'client1' to api.php every whatever its set at via newValue.
                    setInterval(function() {
                        $.get('api.php', 'client=client1', function (newValue) { g1.refresh(newValue); });
                        $.get('api2.php', 'client=client2',function (newValue) { g2.refresh(newValue); });
                        

                    }, 2500);
                };
                // this page recieves the call and gets 50 or 34 or 39 back as a response, and displays the one number for each page call, data was pushed from the ajax call...
            </script>



        </div>
        <div class="card-footer small text-muted"></div>
      </div>

    </div>
    <!-- /.container-fluid-->
    <!-- /.content-wrapper-->
   <?php include "inc/footer.php";?>