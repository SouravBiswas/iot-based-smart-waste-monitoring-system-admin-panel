<?php include 'inc/header.php'?>
<?php include 'classes/Bin.php'?>
<?php
$obj = new Bin();
$allStatus = $obj->citizenstatus();
$number = mysqli_num_rows($allStatus);

?>
<?php
if(isset($_GET['delid'])){
$id = $_GET['delid'];
$obj = new Bin();
$delete = $obj->citizendelete($id);
}
?>
    <script src="vendor/jquery/raphael-2.1.4.min.js"></script>
    <script src="vendor/jquery/justgage.js"></script>
  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumbs-->
     
     
      <!-- Area Chart Example-->
      <div class="card mb-3">
        <div class="card-header">
          <i class="fa fa-bitbucket" ></i>User Details</div>
          <input name="datedepart" type="date"
value="<?php echo date('Y-m-d') ?>"/>
        <div class="card-body">
      
                   <center>
                    <table class="data display datatable" id="example" border="2px">
                        
					<thead>
					    <?php
					    if($number!=0){
					    ?>
						<tr style="text-align: center">
							<th>Serial No.</th>
							<th>Citizen Name</th>
                            <th>Address</th>
                            <th>Mobile</th>
                            <th>Weight</th>
                            <th>Balance</th>
							<th>Action</th>
							
						</tr>
					</thead>
				
					<tbody>
                    <?php
                    
                        $i=0;
                        while ($result = mysqli_fetch_assoc($allStatus)){
                            $i++;
                    ?>
						<tr class="odd gradeX" style="text-align: center">
							<td><?php echo $i?></td>
							<td width="150px"><?php echo $result['name']?></td>
							<td width="250px"><?php echo $result['address']?></td>
							<td width="150px"><?php echo $result['mobile']?></td>
							<td width="75px"><?php echo $result['weight']?></td>
                            <td width="75px"><?php echo $result['balance']?></td>
							<td width="100px"><a href="?delid=<?php echo $result['ID']?>" onclick="return confirm('Are u sure?')">Delete</a></td>
						</tr>
                    <?php } }else{
                       $msg = "<center><span id='message' class='error' style='font-weight: bold;color: red;text-align:center;font-size: 18px'>No Citizen Status Found Yet !!</span></center>";
                       echo $msg;
                    }
                    
                    ?>
					</tbody>
				</table>
				</center>
               

      </div>
      

    </div>
    <!-- /.container-fluid-->
    <!-- /.content-wrapper-->
   <?php include "inc/footer.php";?>